
const button = document.getElementById('buttonInit');

const create = function(tag){
    return document.createElement(tag);
}

//Рандомайзер для цвета ячеек
const colourRandom = function(){
    return  Math.floor(Math.random() * (255 - 0) + 0);
}  

button.addEventListener('click', function(){ 
    if ( document.getElementsByTagName('div').length != 0 ) {
        return;
      }

    let D = prompt('Укажите диаметр круга'); 
    
    const section = create('section');

    document.body.append(section); 

    for(let i = 1; i <= 100; i++){
        let div = create("div"); 
        section.append(div); 
    }

    section.style.cssText = `
        width : ${D * 10}px;
        display : flex;
        flex-wrap: wrap;
    `; 

    const [...divItem] = document.getElementsByTagName('div');
    divItem.forEach(function(divItem){  
        divItem.style.cssText= ` 
            width : ${D}px;
            height : ${D}px;
            background-color : rgb(${colourRandom()},${colourRandom()},${colourRandom()});
            border-radius:50%;
        ` 
    });

    
});



document.addEventListener('click', function(event){ 
    if( event.target.tagName == 'DIV'){
        event.target.remove();
    }else{
        return;
    }
});



